package main

import (
	"fmt"
	"github.com/PuerkitoBio/fetchbot"
	"net/http"
	//"bufio"
	//"io"
	//"github.com/PuerkitoBio/goquery"
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
	"log"
	"time"
	//"os"
	"errors"
	"math/rand"
	"strconv"
	"sync"
	//"strings"
	"strings"
)

var (
	start_addr   = "https://scholar.google.com/scholar?hl=en&as_sdt=0,11&q=tabu+search&oq=ta" //for testing purposes
	front_addr   = "https://scholar.google.com/scholar?start="
	back_addr    = "&q=tabu+search&hl=en&as_sdt=0,11"
	result_count = 10
)

const (
	DATA_ERR = "NO NEW DATA"
	KILL_ERR = "NO LONGER ALIVE"
)

type data []html.Token //data holds (in viewing order) all the results from a page. It is the data in "raw" format that will then be handed off for refining

type HandleWrapper struct { //must implement Handle(*Context, *http.Response, error) to be of interface type Handler
	storage     [][]data    //for storing data
	storageLock uint        //to prevent data leakage; data at a posistion equal to or greater than the lock is locked
	dataLane    chan []data //channel used to transfer data between functions belonging to the struct
	rwLock      *sync.Mutex
	readCounter uint
	alive       bool
}

//storageDealer handles data as it is transferred to and from storage
func (hw *HandleWrapper) storageDealer() {
	valid := true
	var tempData []data
	//var processedData []map[string]string
	for true {
		tempData, valid = <-hw.dataLane
		if !valid {
			hw.alive = false
			return
		}
		//processedData = process(tempData)
		hw.rwLock.Lock()
		hw.storage = append(hw.storage, tempData)
		hw.storageLock++
		hw.rwLock.Unlock()
	}
}

func (hw *HandleWrapper) GetData() (error, []data) {
	hw.rwLock.Lock()
	if hw.readCounter == hw.storageLock {
		hw.rwLock.Unlock()
		if !hw.alive {
			return errors.New(KILL_ERR), nil
		}
		return errors.New(DATA_ERR), nil
	}
	tempData := make([]data, len(hw.storage[hw.readCounter]))
	copy(tempData, hw.storage[hw.readCounter])
	hw.readCounter++
	hw.rwLock.Unlock()
	return nil, tempData
}

func process(rawData []data) (output []map[string]string) {
	output = make([]map[string]string, result_count)
	var tempMap map[string]string
	for i, tok := range rawData {
		tempMap = make(map[string]string) //reset map
		for _, val := range tok {
			//refine data
			switch val.Data {
			case "a":
				if strings.Contains(val.Attr[0].Val, ".pdf") { //the pdf link
					tempMap["pdf"] = val.Attr[0].Val
				} else if strings.Contains(val.Attr[0].Val, "") { //link to the result

				}
			}
		}
		output[i] = tempMap //attach data
	}
	return
}

func main() {
	hw := HandleWrapper{
		storage:     make([][]data, 0),
		storageLock: 0,
		dataLane:    make(chan []data),
		rwLock:      &sync.Mutex{},
		alive:       true,
	}

	go hw.storageDealer()

	go func(hw *HandleWrapper) { // temp function to print data out to console, not 100% working
		var err error
		var dat []data
		time.Sleep(3 * time.Second)
		for true {
			err, dat = hw.GetData()
			if err != nil {
				fmt.Println(err)
				time.Sleep(10 * time.Second)
				continue
			}
			for _, tok := range dat {
				for _, val := range tok {
					fmt.Println(val.Data)
					for _, val2 := range val.Attr {
						fmt.Println("\t<", val2.Key, "> ", val2.Val)
					}
				}
				fmt.Println("~~~NEW DATA~~~")
			}
			//fmt.Println(dat)
			time.Sleep(10 * time.Second)
		}
	}(&hw)

	f := fetchbot.New(fetchbot.HandlerFunc(hw.handler))
	f.DisablePoliteness = true
	//f.UserAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"
	rand.Seed(time.Now().UnixNano())
	sleepTime := time.Duration((10+rand.Intn(50)))*time.Second + time.Duration((rand.Intn(1000)))*time.Millisecond
	queue := f.Start()
	for i := 0; i <= 100; i += result_count { //iterate through 10 pages
		if hw.alive {
			//fmt.Println(front_addr + strconv.Itoa(i) + back_addr)
			fmt.Println("On Results ", i, "-", i+result_count)
			if i == 0 {
				queue.SendStringGet(start_addr)
			} else {
				queue.SendStringGet(front_addr + strconv.Itoa(i) + back_addr)
			}
			fmt.Println("Time to Sleep = ", sleepTime)
			time.Sleep(sleepTime)
			rand.Seed(time.Now().UnixNano())
			sleepTime = time.Duration((10+rand.Intn(60)))*time.Second + time.Duration((rand.Intn(100)))*time.Millisecond
		}
	}
	queue.Close()
}

func (hw *HandleWrapper) handler(ctx *fetchbot.Context, res *http.Response, err error) {
	if err != nil {
		fmt.Println(err)
		return
	}

	z := html.NewTokenizer(res.Body) //for dechipering HTML

outer:
	for {
		tt := z.Next() //peek at next token
		switch tt {    //what kind of token?
		case html.ErrorToken:
			return
		case html.StartTagToken:
			t := z.Token() //store next token
			//fmt.Println(t)
			if t.DataAtom == atom.Div { //if a Div HTML token
				//fmt.Println("Div Token")
				//inner:
				for _, value := range t.Attr { //for each attribute in the Div Token
					//fmt.Print(value.Key, ", ", value.Val, " :: ")
					if value.Key == "id" && value.Val == "gs_res_ccl_mid" {
						//fmt.Println("Found Data")//Debug
						//time.Sleep(10* time.Second)//Debug
						go hw.beginScrape(z)
						break outer
					}
				}
			}
			//case html.EndTagToken:
		}
	}

	//fmt.Println("Listening For Results")
	//for {
	//	test:= <- results
	//	fmt.Println("Result Recieved : Length", len(test))
	//}
}

//beginScrape scrapes the data from a single page.
func (hw *HandleWrapper) beginScrape(z *html.Tokenizer) { //works! as of now...
	//TODO: Work into this function the ability to refine the data rather than just pulling out what I need.
	//resultNum := -1
	results := make([]data, 0)
	depth := 0
	//loop through tokens
//outer:
	for {
		tt := z.Next() //peek at next tokens
		//fmt.Println(tt)
		switch tt {
		case html.ErrorToken:
			log.Println("Error Parsing HTML Token")
			time.Sleep(time.Minute)
		case html.StartTagToken:
			depth++
			t := z.Token() //read next token
			if t.Data == "div" {
				for _, value := range t.Attr {
					if value.Key == "class" && strings.Contains(value.Val, "gs_r gs_or gs_scl") {
						//New result
						fmt.Println("\n")
						break
					} else if value.Key == "class" && strings.Contains(value.Val,"gs_or_ggsm") {
						//PDF result
						datum := depth
						for { //descend another level till you hit data == a
							tt = z.Next()
							if tt == html.StartTagToken {
								depth++
								t = z.Token()
								if len(t.Attr) != 0 && strings.Contains(t.Attr[0].Val, ".pdf") {
									fmt.Println("PDF : ",t.Attr[0].Val) //For now print to screen
								}
							}else if tt == html.EndTagToken{
								depth--
							}

							if depth <= datum{
								break
							}
						}
						break
					} else if value.Key == "class" && strings.Contains(value.Val,"gs_ri"){
						//this is the top html tag of the "meat" of a single result card
						datum := depth
						for {
							tt = z.Next()
							if tt == html.StartTagToken{
								depth++
								t = z.Token()
								if t.Data == "div"{//one of the subcategories
									if strings.Contains(t.Attr[0].Val,"gs_a"){ //author & date
										datum := depth
										//authorFlag := false // author = 1, other = 2
										record:=""
										for {
											tt = z.Next()
											if tt == html.StartTagToken{
												depth++
												t = z.Token()
												if len(t.Attr) != 0 && strings.Contains(t.Attr[0].Key, "href"){
													fmt.Println("Author Link : ", t.Attr[0].Val)
												}
											} else if tt == html.EndTagToken{
												depth--
											} else if tt == html.TextToken{
												t = z.Token()
												record+= t.Data
											}

											if depth < datum{
												//evaluate author string
												temp:=strings.Split(record,"-")
												authors := strings.Split(temp[0],",")
												fmt.Print("Authors : ")
												for _, value := range authors{
													fmt.Print(value + ", ")
												}
												fmt.Println()
												fmt.Print("Citation : ")
												for _, value := range temp[1:]{
													fmt.Print(value + ", ")
												}
												fmt.Println()
												break
											}
										}
									}else if strings.Contains(t.Attr[0].Val,"gs_rs"){
										datum := depth
										for{
											tt = z.Next()
											if tt == html.StartTagToken{
												t = z.Token()
												if t.Data != "br"{
													depth++
												}
												//t = z.Token()
											} else if tt == html.EndTagToken{
												depth--
											} else if tt == html.TextToken{
												t = z.Token()
												fmt.Print(t.Data)
											}

											if depth < datum{
												break
											}
										}
										fmt.Println()
									}else if strings.Contains(t.Attr[0].Val,"gs_fl"){
										datum := depth
										for {
											tt = z.Next()
											if tt == html.StartTagToken{
												depth++
												t = z.Token()
												if len(t.Attr) != 0 && strings.Contains(t.Attr[0].Val, "cluster"){
													clusterID := strings.Split(strings.Split(t.Attr[0].Val, "cluster=")[1],"&")[0]
													fmt.Println("Cluster ID : ", clusterID)
												}
											} else if tt == html.EndTagToken{
												depth--
											}

											if depth < datum{
												break
											}
										}
									}
								} else if t.Data == "h3" && strings.Contains(t.Attr[0].Val,"gs_rt"){ //title of the result
									//fmt.Println(t.Data, " : ", t.Attr)
									//t = z.Token()
									datum := depth
									flag := false
									for {
										tt = z.Next()
										if tt == html.StartTagToken {
											depth++
											t = z.Token()
											if t.Data == "a" && len(t.Attr) != 0 && t.Attr[0].Key == "href"{
												flag = true
												fmt.Println("Title Link : ",t.Attr[0].Val)
											}
											//fmt.Println("Start Tag Token : ", t)
										} else if tt == html.EndTagToken {
											if flag{
												t = z.Token()
												//fmt.Println(t)
												if t.Data == "a"{
													flag = false
												}
											}
											depth--
										} else if tt == html.TextToken {
											if flag{
												t = z.Token()
												fmt.Print("Title : ", t.Data)
											}
										}

										if depth < datum{
											break
										}
									}
									fmt.Println("")
								}
							} else if tt == html.EndTagToken {
								depth--
							}

							if depth < datum{
								break
							}
						}
						break
					}
				}
			}
		case html.EndTagToken: //do nothing?
			depth--
		default:
		}
		if depth < 0{
			break
		}
	}
	hw.dataLane <- results
}

/*
!!!GOOGLE CHANGED THEIR HTML SO IT MAY BREAK!!!
Add in the ability to check against what currently is happening.
Have file which reads in the attributes to look for

find start of all results -> gs_ccl_results
till no more results (go back to same level as the start of results) -> gs_r
	//the following is one result
	get	block -> gs_ri
		get title gs_rt -> <a href>
		get Citations gs_a -> <a href>
		get abstract gs_rs
	get pdf link -> gs_ggs gs_fl
		gs_ggsm gs_anm -> gs_ggsd -> <a href> "link ends in pdf"
//to increase the next page make start=0 start={(page#-1)*10}
//you know you have exhausted the results when gs_ccl_results is empty
*/
